#include <windows.h>
#include "detours/Detours.h"
#include "plugin.h"
#include <string>

using namespace std;

#define PLUGINNAME L"LabelArgs"
#define VERSION L"v1.0"

typedef int (__cdecl* DecodeknownbyaddrPtr)(ulong addr, t_procdata* pd, t_argdec adec[NARG], wchar_t* rettype, wchar_t* name, int nexp, int follow);
DecodeknownbyaddrPtr OldDecodeknownbyaddr;

static int Mabout(t_table* pt, wchar_t* name, ulong index, int mode)
{
    if(mode == MENU_VERIFY)
        return MENU_NORMAL;
    else if(mode == MENU_EXECUTE)
    {
        Resumeallthreads();
        MessageBox(hwollymain, L"Original Plugin by: High6\nPorted by: Mr. eXoDia", PLUGINNAME L" " VERSION, MB_OK | MB_ICONINFORMATION);
        Suspendallthreads();
        return MENU_NOREDRAW;
    }
    return MENU_ABSENT;
}

static void put(const wchar_t* string)
{
#ifndef DEBUG
    return;
#endif //DEBUG
    if(!string)
        return;
    static bool hasConsole = false;
    if(!hasConsole)
    {
        hasConsole = true;
        AllocConsole();
    }
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    DWORD written = 0;
    WriteConsoleW(hConsole, string, lstrlenW(string), &written, 0);
    written = 0;
    WriteConsoleW(hConsole, "\n", 1, &written, 0);
}

static int Mtest(t_table* pt, wchar_t* name, ulong index, int mode)
{
    if(mode == MENU_VERIFY)
        return MENU_NORMAL;
    else if(mode == MENU_EXECUTE)
    {
        Resumeallthreads();
        t_argdec adec[NARG];
        memset(adec, 0, sizeof(adec));
        for(int i = 0; i < NARG; i++)
        {
            adec[i].mode = ADEC_VALID;
        }
        ulong addr = 0x40BB7C;
        //ulong addr=0x415186;
        wchar_t label[TEXTLEN] = L"";
        int args = Decodeknownbyaddr(addr, 0, adec, 0, 0, -1, 1);
        if(args == -1)
        {
            MessageBox(hwollymain, L"Decodeknownbyaddr returned -1", L"ERROR", 0);
        }
        else
        {
            FindnameW(addr, NM_LABEL, label, TEXTLEN);
            wchar_t output[2048] = L"";
            wsprintfW(output, L"function %X:\n  label: L\"%s\"\n  %d arguments\n", addr, label, args);
            put(output);
            for(int i = 0; i < args; i++)
            {
                wsprintfW(output, L"    argument%d:\n        name: L\"%s\"\n        type: L\"%s\"", i + 1, adec[i].name, adec[i].prtype);
                put(output);
            }
            put(L"");
        }
        Suspendallthreads();
        return MENU_NOREDRAW;
    }
    return MENU_ABSENT;
}

wstring & Trim(wstring & str)
{
    wstring::size_type pos = str.find_last_not_of(' ');
    if(pos != wstring::npos)
    {
        str.erase(pos + 1);
        pos = str.find_first_not_of(' ');
        if(pos != wstring::npos) str.erase(0, pos);
    }
    else str.erase(str.begin(), str.end());

    return str;
}

int MyDecodeknownbyaddr(ulong addr, t_procdata* pd, t_argdec adec[NARG], wchar_t* rettype, wchar_t* name, int nexp, int follow)
{
    int result = OldDecodeknownbyaddr(addr, pd, adec, rettype, name, nexp, follow);
    if(result != -1)
    {
        wchar_t name[TEXTLEN] = L"";
        int len;
        if(len = FindnameW(addr, NM_LABEL, name, TEXTLEN))
        {
            put(name);
            wstring label = wstring(name, len);
            int st;
            int en;
            if((st = label.find(L'(')) != -1 && (en = label.find(L')')) != -1)
            {
                wchar_t output[20] = L"";
                wsprintfW(output, L"st=%d,en=%d", st, en);
                put(output);
                wstring id;
                int argnum = 0;
                for(int i = st + 1; i < en; i++)
                {
                    if(label[i] == L',')
                    {
                        put(id.c_str());
                        wcscpy_s(adec[argnum].name, Trim(id).c_str());
                        argnum++;
                        id = L"";
                    }
                    else
                    {
                        id += label[i];
                    }
                }
                wcscpy_s(adec[argnum].name, Trim(id).c_str());
                put(id.c_str());
            }
        }
    }
    return result;
}

static t_menu mainmenu[] =
{
#ifdef DEBUG
    {
        L"Test",
        L"Test",
        K_NONE, Mtest, NULL, 0
    },
#endif
    {
        L"About "PLUGINNAME,
        L"About Bookmarks plugin",
        K_NONE, Mabout, NULL, 0
    },
    { NULL, NULL, K_NONE, NULL, NULL, 0 }
};

extc t_menu* __cdecl ODBG2_Pluginmenu(wchar_t* type)
{
    if(wcscmp(type, PWM_MAIN) == 0)
        return mainmenu;
    return NULL;
}

extc int __cdecl ODBG2_Pluginquery(int ollydbgversion, ulong* features, wchar_t pluginname[SHORTNAME], wchar_t pluginversion[SHORTNAME])
{
    if(ollydbgversion < 201)
        return 0;
    wcscpy_s(pluginname, SHORTNAME, PLUGINNAME);
    wcscpy_s(pluginversion, SHORTNAME, VERSION);
    return PLUGIN_VERSION;
}

BOOL WINAPI DllMain(HINSTANCE hi, DWORD reason, LPVOID reserved)
{
    if(reason == DLL_PROCESS_ATTACH)
    {
        OldDecodeknownbyaddr = (DecodeknownbyaddrPtr)DetourFunction((PBYTE)Decodeknownbyaddr, (PBYTE)&MyDecodeknownbyaddr);
    }
    else if(reason == DLL_PROCESS_DETACH)
    {
        DetourRemove((PBYTE)OldDecodeknownbyaddr, (PBYTE)&MyDecodeknownbyaddr);
    }
    return TRUE;
}